# coding: utf8
import os
import torch


class Config:
    train_batch_size = 32
    dev_batch_size = 64
    test_batch_size = 64
    max_length = 128
    epochs = 20
    lr = 1e-5
    model_save_dir = os.path.normpath(os.path.join(os.path.dirname(__file__), "../checkpoints"))
    load_model = True
    best_model_path = os.path.join(model_save_dir, "se2seq_epoch_1.pt")
    #
    gpu_ids = '1,2,3'


opt = Config()
