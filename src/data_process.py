# coding: utf8
import os
from random import shuffle, sample
from torch.utils.data import Dataset
from src.config import opt

project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "../"))


def load_dialog(train_radio=0.6, test_radio=0.25, is_shuffle=False, sampling=None):
    f = os.path.join(project_dir, "data/train.txt")
    with open(f, encoding="utf8")as fi:
        data = fi.read()
        data = data.strip().split("\n\n")
    #
    if isinstance(sampling, int):
        data = sample(data, sampling)
    if is_shuffle:
        shuffle(data)
    pos_a = int(len(data)*train_radio)
    pos_b = int(len(data)*(train_radio+test_radio))
    train, dev, test = data[:pos_a], data[pos_b:], data[pos_a:pos_b]
    return train, dev, test


class DialogDataset(Dataset):
    def __init__(self, data):
        self.pairs = []
        for d in data:
            self.pairs.extend(self._dialog2seq_pairs(d))

    def _dialog2seq_pairs(self, dialog):
        seqs = dialog.strip().split("\n")
        max_seqs = 8
        min_seqs = 2
        if len(seqs) < min_seqs:
            return []
        pairs = []
        for length in range(min_seqs, max_seqs+1):
            for i in range(len(seqs)-length+1):
                sub_seqs = seqs[i:i+length]
                pairs.append(("\n".join(sub_seqs[0:-1]), sub_seqs[-1]))
        return pairs

    def __getitem__(self, item):
        return self.pairs[item]

    def __len__(self):
        return len(self.pairs)


if __name__ == "__main__":
    train, dev, test = load_dialog()
    train_gen = DialogDataset(train)
    dev_gen = DialogDataset(dev)
    test_gen = DialogDataset(test)
    print("train: {}, dev: {}, test: {}".format(len(train_gen), len(dev_gen), len(test_gen)))
    print(train_gen[0])
