# coding: utf8
import os
import sys
import torch
from asian_bart import AsianBartTokenizer, AsianBartForConditionalGeneration
from transformers.models.bart.modeling_bart import shift_tokens_right
from torch.optim import Adam
from datetime import datetime
from collections import OrderedDict

from src.config import opt

gpu_ids = opt.gpu_ids
# os.environ["CUDA_VISIBLE_DEVICES"] = "{}".format(gpu_ids)

gpu_ids = [int(i) for i in gpu_ids.split(",")]
device = torch.device("cuda:{}".format(gpu_ids[0]) if torch.cuda.is_available() else "cpu")
print("current device: {}".format(device))
sys.stdout.flush()


class Seq2Seq:
    def __init__(self):
        # create model and tokenizer
        self.tokenizer = AsianBartTokenizer.from_pretrained("hyunwoongko/asian-bart-zh")
        self.model = AsianBartForConditionalGeneration.from_pretrained("hyunwoongko/asian-bart-zh")
        if torch.cuda.device_count() > 1:
            self.model = torch.nn.DataParallel(self.model, device_ids=gpu_ids).to(device)
        else:
            self.model.to(device)
        learning_rate = opt.lr
        self.optimizer = Adam(self.model.parameters(), lr=learning_rate)
        print("{}, 成功初始化模型.".format(datetime.now()))
        sys.stdout.flush()

    def train(self, train_data, dev_data, test_data, **kwargs):
        print("{}, 开始训练...".format(datetime.now()))
        sys.stdout.flush()
        epochs = opt.epochs
        model_save_dir = opt.model_save_dir
        best_dev_value = float("inf")
        for epoch in range(epochs):
            print("epoch: {}/{}".format(epoch+1, epochs))
            sys.stdout.flush()
            #
            self.model.train()
            train_loss_list = []
            train_length = len(train_data)
            i = 0
            for input_seqs, output_seqs in train_data:
                i += 1
                if len(input_seqs) == 0:
                    continue
                self.optimizer.zero_grad()
                loss, _ = self.forward(input_seqs, output_seqs)
                loss.backward()
                train_loss_list.append(loss.item())
                print("\t{}, {}/{}, training loss: {}".format(
                    datetime.now(), i, train_length, round(train_loss_list[-1], 5)))
                sys.stdout.flush()
                self.optimizer.step()
                torch.cuda.empty_cache()
            print("\t{}, training is done, evaluation on dev is started...".format(datetime.now()))
            sys.stdout.flush()
            avg_train_loss = sum(train_loss_list) / len(train_loss_list)
            self.model.eval()
            # dev
            dev_loss_list = []
            dev_length = len(dev_data)
            i = 0
            for dev_input_seqs, dev_out_seqs in dev_data:
                i += 1
                if len(dev_input_seqs) == 0:
                    continue
                loss, _ = self.forward(dev_input_seqs, dev_out_seqs)
                dev_loss_list.append(loss.item())
                print("\t{}, {}/{}, dev loss: {}".format(
                    datetime.now(), i, dev_length, round(dev_loss_list[-1], 5)))
                sys.stdout.flush()
                torch.cuda.empty_cache()
            avg_dev_loss = sum(dev_loss_list) / len(dev_loss_list)
            # test
            test_loss_list = []
            test_length = len(test_data)
            i = 0
            for test_input_seqs, test_out_seqs in test_data:
                i += 1
                if len(test_input_seqs) == 0:
                    continue
                loss, _ = self.forward(test_input_seqs, test_out_seqs)
                test_loss_list.append(loss.item())
                print("\t{}, {}/{}, test loss: {}".format(
                    datetime.now(), i, test_length, round(test_loss_list[-1], 5)))
                sys.stdout.flush()
                torch.cuda.empty_cache()
            avg_test_loss = sum(test_loss_list) / len(test_loss_list)
            print("{}, epoch: {}/{}, 训练集平均loss {:.5f}: , 验证集平均loss: {:.5f}, 测试集平均loss: {:.5f}".format(
                datetime.now(), epoch+1, epochs, avg_train_loss, avg_dev_loss, avg_test_loss))
            sys.stdout.flush()
            #
            if avg_dev_loss < best_dev_value:
                best_dev_value = avg_dev_loss
                save_path = os.path.join(model_save_dir, "se2seq_epoch_{}.pt".format(epoch))
                torch.save(self.model.state_dict(), save_path)
                print("{}, 发现更优模型, 保存到：{}".format(datetime.now(), save_path))
                sys.stdout.flush()

    def forward(self, input_seq_list, output_seq_list):
        # tokenize texts
        tokens = self.tokenizer.prepare_seq2seq_batch(
             src_texts=input_seq_list,
             src_langs="zh_CN",
             tgt_texts=output_seq_list,
             tgt_langs="zh_CN",
             max_len=opt.max_length
         )
        input_ids = tokens["input_ids"]
        input_ids = input_ids.to(device)
        attention_mask = tokens["attention_mask"]
        attention_mask = attention_mask.to(device)
        labels = tokens["labels"]
        labels = labels.to(device)
        decoder_input_ids = shift_tokens_right(labels, self.tokenizer.pad_token_id,
                                               self.tokenizer.lang_code_to_id["zh_CN"])
        decoder_input_ids = decoder_input_ids.to(device)
        # forwarding model for training
        outputs = self.model(
             input_ids=input_ids,
             attention_mask=attention_mask,
             decoder_input_ids=decoder_input_ids,
         )
        # compute loss
        lm_logits = outputs[0]
        loss_function = torch.nn.CrossEntropyLoss(
             ignore_index=self.tokenizer.pad_token_id
         )
        loss = loss_function(
             lm_logits.view(-1, lm_logits.shape[-1]),
             labels.view(-1)
         )
        return loss, lm_logits

    def generate(self, input_seq_list):
        print("{}, 开始生成...".format(datetime.now()))
        sys.stdout.flush()
        # generate text
        self.model.eval()
        # tokenize texts
        tokens = self.tokenizer.prepare_seq2seq_batch(
            src_texts=input_seq_list,
            src_langs="zh_CN",
            max_len=opt.max_length
        )
        print("{}, token化完成.".format(datetime.now()))
        sys.stdout.flush()
        input_ids = tokens["input_ids"]
        input_ids.to(device)
        attention_mask = tokens["attention_mask"]
        attention_mask.to(device)
        start_token_id = self.tokenizer.lang_code_to_id["zh_CN"]
        # start_token_id.to(device)
        output = self.model.generate(
            input_ids=input_ids,
            attention_mask=attention_mask,
            decoder_start_token_id=start_token_id,
        )
        output = output.squeeze(0)
        output_tokens = self.tokenizer.decode(output)
        print("{}, 文本生成完成.".format(datetime.now()))
        sys.stdout.flush()
        return output_tokens

    def load_model(self, model_path):
        state_dict = torch.load(model_path, map_location=device)
        try:
            self.model.load_state_dict(state_dict)
        except RuntimeError as e:
            print("{}, 可能是多卡训练模型，尝试将多卡训练的模型转化为单卡或者cpu可加载模型.".format(datetime.now()))
            new_state_dict = OrderedDict()
            for k, v in state_dict.items():
                name = k[7:]
                new_state_dict[name] = v
            self.model.load_state_dict(new_state_dict)
        print("{}, 成功加载现有模型.".format(datetime.now()))
        sys.stdout.flush()

    def pre_run(self):
        """
        模型预热，避免初次计算时非常耗时的问题
        :return:
        """
        print("{}, 开始模型预热...".format(datetime.now()))
        _ = self.generate("预热")
        print("{}, 模型预热结束.".format(datetime.now()))


if __name__ == "__main__":
    m = Seq2Seq()
    t = "你好啊"
    m.pre_run()
    print(m.generate(t).shape)
