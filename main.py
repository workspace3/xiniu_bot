# coding: utf8
import os
import sys
from datetime import datetime
from torch.utils.data import DataLoader

project_dir = os.path.abspath(os.path.normpath(os.path.join(os.path.dirname(__file__), "./")))
print(project_dir)

sys.path.insert(0, project_dir)

from src.modules.seq2seq import Seq2Seq
from src.config import opt
from src.data_process import DialogDataset, load_dialog


def main():
    train, dev, test = load_dialog(sampling=200, is_shuffle=True)
    train_set = DialogDataset(train)
    dev_set = DialogDataset(dev)
    test_set = DialogDataset(test)
    train_loader = DataLoader(train_set, batch_size=opt.train_batch_size)
    dev_loader = DataLoader(dev_set, batch_size=opt.dev_batch_size)
    test_loader = DataLoader(test_set, batch_size=opt.test_batch_size)
    del train, dev, test, train_set, dev_set, test_set
    print("{}, 加载数据完成. 训练/验证/测试：{}/{}/{}".format(
        datetime.now(), len(train_loader), len(dev_loader), len(test_loader)))
    handler = Seq2Seq()
    if opt.load_model:
        handler.load_model(opt.best_model_path)
    else:
        handler.train(train_loader, dev_loader, test_loader)
    #
    handler.pre_run()
    t = "y"
    while "y" == t:
        print("-"*30)
        s = input("{}, 用户输入：".format(datetime.now()))
        r = handler.generate(s)
        print("{}, 系统回复：{}".format(datetime.now(), r))
        print("-"*30)
        t = input("是否继续？y/n")
    print("Done.")


if __name__ == "__main__":
    main()
